/**
 * 基础路由
 * @type { *[] }
 */

const constantRouterMap = [
  {
    path: '/',
    name: 'Example',
    redirect: { name: 'AudioIndex' },
    children: [
      {
        path: '/example',
        name: 'ExampleHelloIndex',
        component: () => import('@/views/example/hello/Index.vue')
      },
      {
        path: '/audio',
        name: 'AudioIndex',
        component: () => import('@/views/audio/TextToAudio.vue')
      },
    ]
  },
]

export default constantRouterMap
