'use strict';

const { Service } = require('ee-core');


/**
 * audio 文本转语音
 */
class AudioService extends Service {

	/**
	 * 构造器
	 */
	constructor(ctx) {
		super(ctx);
	}

	/**
	 * getAudioToText
	 */
	async getAudioToText(args) {
		let obj = {
			status:'ok',
			params: args
		}

		return obj;
	}
}

AudioService.toString = () => '[class AudioService]';
module.exports = AudioService;
