'use strict';

const {Controller} = require('ee-core');
const Log = require('ee-core/log');
const Services = require('ee-core/services');

/**
 * audio 文本转语音
 */
class AudioController extends Controller {

	/**
	 * 构造器
	 */
	constructor(ctx) {
		super(ctx);
	}


	/**
	 * 所有方法接收两个参数
	 * @param args 前端传的参数
	 * @param event - ipc通信时才有值。详情见：控制器文档
	 */

	/**
	 * test
	 */
	async test(args, event) {
		// 前端参数
		console.log("electron/controller/audio: ", args);
		// 调用服务层Service
		const result = await Services.get('audioService').getAudioToText(args);
		Log.info('service result:', result);


		// 主动向前端发请求
		// channel 前端ipc.on()，监听的路由
		const channel = "channel-test"
		// IpcMainEvent
		event.sender.send(`${channel}`, {msg: ' 我是主动向前端发请求的数据'})

		// 返回数据
		return {
			msg: result
		};
	}

	/**
	 * 测试双向通信
	 * @param args
	 * @param event
	 * @returns {Promise<{msg: string}>}
	 */
	async getAudioToText(args, event) {
		// IpcMainInvokeEvent
		event.reply("getAudioToText", {args: args, msg: '我是双向通信的数据'})
	}


}

AudioController.toString = () => '[class AudioController]';
module.exports = AudioController;
